package cn.siminxu.business.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author siminx@163.com
 * @date  2018/7/25 20:47
 */
@RestController
@RequestMapping("api")
public class DemoController {

    @GetMapping("demo")
    public String demo() {
        return "Hello Api Demo";
    }
}
